# Traditional Fighter Template #

An ActionScript 3 template for prototyping traditional fighter style games. Uses the Flixel game framework.

[Try it out](http://christopherdcanfield.com/projects/ludumdare/30/templates/fighter/).

![traditional-fighter.jpg](https://bitbucket.org/repo/do58bp/images/2565831678-traditional-fighter.jpg)

Christopher D. Canfield  
September 2014