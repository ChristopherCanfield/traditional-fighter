package com.divergentthoughtsgames.assets 
{
	/**
	 * ...
	 * @author Christopher D. Canfield
	 */
	public class Graphics 
	{
		[Embed(source = '../../../../res/art/zelda-chickens.png')]
		public const Chickens: Class;
		
		[Embed(source = '../../../../res/art/stick-man.png')]
		public const StickMan: Class;
		
		[Embed(source = '../../../../res/art/arena.png')]
		public const Arena: Class;
	}
}