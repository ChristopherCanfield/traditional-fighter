package com.divergentthoughtsgames 
{
	import org.flixel.FlxGroup;
	import org.flixel.FlxObject;
	import org.flixel.FlxPoint;
	import org.flixel.FlxSprite;
	import org.flixel.FlxState;
	import org.flixel.FlxTilemap;
	import org.flixel.FlxG;
	import org.flixel.FlxEmitter;
	import org.flixel.FlxParticle;
	import org.flixel.plugin.photonstorm.FlxBar;
	import org.flixel.plugin.photonstorm.FlxWeapon;
	
	import com.divergentthoughtsgames.assets.Assets;
	
	/**
	 * The player.
	 * @author Christopher D. Canfield
	 */
	public class Player extends FlxSprite
	{		
		public static const HEIGHT: uint = 207;
		public static const WIDTH: uint = 145;
		
		private var playerNumber: int;
		
		private var startX: int;
		private var startY: int;
		
		private var gameState: FlxState;
		
		private var healthBar:FlxBar;
		
		private var lives:int = 3;
		
		public var particleGroup: FlxGroup = new FlxGroup(200);
		
		private var punch: Attack;
		
		public function Player(gameState: FlxState, attackGroup: FlxGroup, startX: int, startY: int, playerNumber: int) 
		{
			this.startX = startX;
			this.startY = startY;
			
			this.gameState = gameState;
			this.playerNumber = playerNumber;
			
			health = 100;
			
			x = startX;
			y = startY;
			maxVelocity.x = 550;
			maxVelocity.y = 500;
			acceleration.y = 700;
			drag.x = maxVelocity.x * 8;
			
			facing = (playerNumber == 1) ? FlxObject.RIGHT : FlxObject.LEFT;
			
			var image:FlxSprite = loadGraphic(Assets.graphics.StickMan, true, true, WIDTH, HEIGHT);
			addAnimation("stop", [0, 1, 2], 8, true);
			addAnimation("run", [3, 4, 5, 6, 7, 8], 14, true);
			addAnimation("punch", [9, 10, 11, 12], 11, false);
			play("stop");
			
			// Adjust bounding box.
			width = WIDTH - 70;
			offset.x = 30;
			
			// Add health bar.
			healthBar = new FlxBar(0, 0, FlxBar.FILL_LEFT_TO_RIGHT, width, 4, this, "health");
			healthBar.trackParent(0, -5);
			gameState.add(healthBar);
			
			punch = new Attack(this, attackGroup, "punch");
			punch.minDamage = 5;
			punch.maxDamage = 12;
			punch.projectileHeight = punch.projectileWidth = 3;
			punch.speed = 1000;
			punch.range = 50;
			// Specifies whether a visualization is drawn for the attack.
			punch.debug = false;
		}
		
		public function getLives():int
		{
			return lives;
		}
	
		override public function update():void
		{
			if (health <= 0)
			{
				//lives--;
				//resetAtStartPosition();
				
				explode();
				healthBar.kill();
				kill();
				return;
			}
			
			health += 0.05;
			processUserInput();
		}
		
		private function processUserInput():void
		{
			acceleration.x = 0;
			
			if ((FlxG.keys.A && playerNumber == 1) || (FlxG.keys.LEFT && playerNumber == 2))
			{
				acceleration.x = -maxVelocity.x * 8;
				facing = FlxObject.LEFT;
			}
			if ((FlxG.keys.D && playerNumber == 1) || (FlxG.keys.RIGHT && playerNumber == 2))
			{
				acceleration.x = maxVelocity.x * 8;
				facing = FlxObject.RIGHT;
			}
			
			if (((FlxG.keys.W && playerNumber == 1) || (FlxG.keys.UP && playerNumber == 2))
					&& isTouching(FlxObject.FLOOR))
			{
				jump();
			}
			
			if ((FlxG.keys.E && playerNumber == 1) || (FlxG.keys.SHIFT && playerNumber == 2))
			{
				attack(punch);
			}
			
			animate();
		}
		
		override public function reset(x:Number, y:Number):void
		{
			super.reset(x, y);
			
			this.acceleration.x = 0;
			this.acceleration.y = 200;
			this.health = 100;
		}
		
		/**
		 * Resets the player at the starting position.
		 */
		private function resetAtStartPosition():void
		{
			reset(startX, startY);
		}
		
		private function animate(): void
		{
			if (_curAnim.name != punch.animationName)
			{
				if (velocity.x != 0 || velocity.y != 0)
				{
					play("run");
				} 
				else 
				{ 
					play("stop");
				}
			}
		}
		
		private function jump(): void
		{
			velocity.y = -maxVelocity.y * 0.75;
			//FlxG.play(Assets.audio.Jump, 0.25);
		}
		
		private function attack(attack: Attack): void
		{
			if (attack.ready)
			{
				play(attack.animationName);
				addAnimationCallback(onAttackStop);
				attack.execute();
				
				var direction: int = (facing == RIGHT) ? 1 : -1;
				acceleration.x = maxVelocity.x * 2 * direction;
			}
		}
		
		/**
		 * 
		 * @param	name
		 * @param	frameNumber the frame number of the animation.
		 * @param	frameIndex the frame number in the entire sprite sheet.
		 */
		private function onAttackStop(name: String, frameNumber: uint, frameIndex: uint): void
		{
			if (frameNumber == _curAnim.frames.length - 1)
			{
				if (name == punch.animationName)
				{
					punch.ready = true;
				}
				
				play("run");
				addAnimationCallback(null);
			}
		}
		
		private function explode(): void
		{
			var emitter: FlxEmitter = new FlxEmitter(x, y);
			var particles:int = 200;
 
			for(var i:int = 0; i < particles; i++)
			{
				var particle:FlxParticle = new FlxParticle();
				particle.makeGraphic(5, 5, 0xaa7f0000);
				particle.exists = false;
				emitter.add(particle);
				particleGroup.add(particle);
			}
			
			emitter.gravity = 400;
			emitter.maxParticleSpeed = new FlxPoint(400, 400);
			emitter.bounce = 0.1;
			
			gameState.add(emitter);
			emitter.start(true, 8);
		}
	}
}